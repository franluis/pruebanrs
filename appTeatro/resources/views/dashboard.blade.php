@extends('layouts.app')

@section('title', 'Panel | TEATRO')

@section('main-navbar')
<header class="header">
    <nav class="navbar">

        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
          
                <!-- Navbar Header-->
                <div class="navbar-header">
                
                    <!-- Navbar Brand -->
                    <a href="{{ route('index') }}" class="navbar-brand d-none d-sm-inline-block">
                        <div class="brand-text d-none d-lg-inline-block"><span>Panel </span><strong>Teatro</strong></div>
                        <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>P|Teatro</strong></div>
                    </a>
                
                    <!-- Toggle Button-->
                    <a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                </div>

            </div>
        </div>
    </nav>
</header>
@endsection

@section('side-navbar')
<nav class="side-navbar">

    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            <img src="{{ asset('img/avatar_user.jpg') }}" alt="..." class="img-fluid rounded-circle">
        </div>
        <div class="title">
            <h1 class="h4">Sonia Silva</h1>
            <p>Bienvenida</p>
        </div>
    </div>

    <!-- Sidebar Navidation Menus-->
    <ul class="list-unstyled">
        <li class="active"><a href="{{ route('index') }}"> <i class="icon-home"></i>Inicio </a></li>
        <li><a href="{{ route('users.index') }}"> <i class="fa fa-users" aria-hidden="true"></i>Clientes </a></li>
        <li><a href="{{ route('reservation.index') }}"> <i class="fa fa-ticket" aria-hidden="true"></i>Reservas </a></li>
    </ul>
</nav>
@endsection

@section('content-inner')
<div class="content-inner">
    
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Panel Administrador</h2>
        </div>
    </header>

    <!-- Dashboard Counts Section-->
    <section class="dashboard-counts no-padding-bottom">
        <div class="container-fluid">
            <div class="row bg-white has-shadow">

                <!-- Item -->
                <div class="col-xl-6 col-sm-6">
                    <div class="item d-flex align-items-center">
                        <div class="icon bg-green"><i class="fa fa-users" aria-hidden="true"></i></div>
                        <div class="title">
                            <span>Clientes<br>Registrados</span>
                            <div class="progress">
                                <div role="progressbar" style="width: 20%; height: 4px;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                            </div>
                        </div>
                        <div class="number"><strong>{{ $countUsers }}</strong></div>
                    </div>
                </div>
                
                <!-- Item -->
                <div class="col-xl-6 col-sm-6">
                    <div class="item d-flex align-items-center">
                        <div class="icon bg-orange"><i class="fa fa-ticket" aria-hidden="true"></i></div>
                        <div class="title">
                            <span>Reservas<br>Registradas</span>
                            <div class="progress">
                                <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                            </div>
                        </div>
                        <div class="number"><strong>{{ $countReservations }}</strong></div>
                    </div>
                </div>
          
            </div>
        </div>
    </section>

    <!-- Projects Section-->
    <section class="projects">
        <div class="container-fluid">

            @foreach($listTheaterplay as $theaterplay)
            <!-- Project-->
            <div class="project">
                <div class="row bg-white has-shadow">
                    <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
                        <div class="project-title d-flex align-items-center">
                            <div class="image has-shadow"><img src="http://t1.gstatic.com/images?q=tbn:ANd9GcRwQUPHlv9XT7-8IBSCjKWZx7RvZZdu-_qFn0LvPoMXUFmNmdqy" alt="..." class="img-fluid"></div>
                            <div class="text">
                                <h3 class="h4">{{ $theaterplay['name'] }}</h3><small>{{ $theaterplay['author'] }}</small>
                            </div>
                        </div>
                        <div class="project-date"><span class="hidden-sm-down">{{ $theaterplay['date'] }}</span></div>
                    </div>
                    <div class="right-col col-lg-6 d-flex align-items-center">
                        <!-- <div class="time"><i class="fa fa-clock-o"></i>{{ $theaterplay['date'] }}</div> -->
                        <div class="comments"><i class="fa fa-exchange" aria-hidden="true"></i>Asientos: 50</div>
                        <div class="project-progress">
                            <div class="progress">
                                <div role="progressbar" style="width: 70%; height: 6px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </section>
      
    <!-- Page Footer-->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>NRS GROUP &copy; 2018</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Desarrollado por <a href="https://www.linkedin.com/in/francisco-gerardino/" class="external">Francisco Gerardino</a></p>
                </div>
            </div>
        </div>
    </footer>

</div>
@endsection