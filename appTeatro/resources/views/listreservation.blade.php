@extends('layouts.app')

@section('title', 'Panel | TEATRO')

@section('main-navbar')
<header class="header">
    <nav class="navbar">

        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
          
                <!-- Navbar Header-->
                <div class="navbar-header">
                
                    <!-- Navbar Brand -->
                    <a href="{{ route('index') }}" class="navbar-brand d-none d-sm-inline-block">
                        <div class="brand-text d-none d-lg-inline-block"><span>Panel </span><strong>Teatro</strong></div>
                        <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>P|Teatro</strong></div>
                    </a>
                
                    <!-- Toggle Button-->
                    <a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                </div>

            </div>
        </div>
    </nav>
</header>
@endsection

@section('side-navbar')
<nav class="side-navbar">

    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            <img src="{{ asset('img/avatar_user.jpg') }}" alt="..." class="img-fluid rounded-circle">
        </div>
        <div class="title">
            <h1 class="h4">Sonia Silva</h1>
            <p>Bienvenida</p>
        </div>
    </div>

    <!-- Sidebar Navidation Menus-->
    <ul class="list-unstyled">
        <li><a href="{{ route('index') }}"> <i class="icon-home"></i>Inicio </a></li>
        <li><a href="{{ route('users.index') }}"> <i class="fa fa-users" aria-hidden="true"></i>Clientes </a></li>
        <li class="active"><a href="{{ route('reservation.index') }}"> <i class="fa fa-ticket" aria-hidden="true"></i>Reservas </a></li>
    </ul>
</nav>
@endsection

@section('content-inner')
<div class="content-inner">
    
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Gestion de Reservas</h2>
        </div>
    </header>

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Reservas</li>
        </ul>
    </div>
    
    <section class="tables">   
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-close">
                            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-success" id="btn-register-reservation"  data-length="{{ count($listTheaterplay) }}" ><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                        </div>
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Listado de Reservas</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Obra de Teatro</th>
                                            <th>Fecha</th>
                                            <th>Estatus</th>
                                            <th colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($listReservation as $reservation)
                                        <tr>
                                            <th scope="row">{{ $reservation->id }}</th>
                                            <td>{{ $reservation->firstname }}</td>
                                            <td>{{ $reservation->email_users }}</td>
                                            <td>{{ $reservation->name }}</td>
                                            <td>{{ $reservation->date }}</td>
                                            <td>{{ $reservation->status }}</td>                                            
                                            <td><button type="button" class="btn btn-xs btn-warning" id="button-detail_{{ $reservation->id }}" data-object="{{ $reservation->id }}" data-toggle="modal" data-target="#myModalUpdate" ><i class="fa fa-pencil" aria-hidden="true"></i></button></td>
                                            <td><button type="button" class="btn btn-xs btn-danger" id="button-delete_{{ $reservation->id }}" data-object="{{ $reservation->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button></td>                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      
    <!-- Page Footer-->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>NRS GROUP &copy; 2018</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Desarrollado por <a href="https://www.linkedin.com/in/francisco-gerardino/" class="external">Francisco Gerardino</a></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modal Mensaje Validation -->
    <div class="modal fade" id="modal-messaje-validate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messaje-validate"></h5>
                    <button type="button" class="btn btn-danger" id="button-validate-reservation" data-id="" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('reservation.store') }}" >
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Nueva Reserva</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body" id="body-register-reservation">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select name="client" id="input-client" class="form-control mb-3">
                                        @foreach($listClient as $client)
                                        <option value="{{ $client['email'] }}" >{{ $client['firstname'] }} {{ $client['lastname'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Obra de Teatro</label>
                                    <select name="theaterplay" id="input-theaterplay" class="form-control mb-3">
                                        @foreach($listTheaterplay as $theaterplay)
                                        <option value="{{ $theaterplay->id }}">{{ $theaterplay->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fecha</label>
                                    <input type="text" name="date" id="input-date" class="form-control" readonly="true" value="{{ $dateCurrent }}" >
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Fila</label>
                                    <select name="row[]" id="input-row" class="form-control mb-3">
                                        @for ($i = 0; $i < 5; $i++)
                                        <option value="{{ $i+1 }}">{{ $i+1 }}</option>
                                        @endfor    
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Columna</label>
                                    <select name="column[]" id="input-column" class="form-control mb-3">
                                        @for ($i = 0; $i < 10; $i++)
                                        <option value="{{ $i+1 }}" >{{ $i+1 }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="button-agregate-seat" >Agregar Butaca</button>
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="button-register-reservation" data-result="{{ $errorRegister }}">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Mensaje Registro -->
    <div class="modal fade" id="modal-messaje-exito" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messaje-register"></h5>
                    <button type="button" class="btn btn-success" id="btn-messaje-register" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Mensaje Eliminar -->
    <div class="modal fade" id="modal-messaje-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messaje-delete"></h5>
                    <button type="button" class="btn btn-danger" id="button-delete-reservation" data-id="" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Modificar -->
    <div id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <form method="PUT" action="" >
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Detalles de la Reserva</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body" id="body-update-reservation">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select name="clientu" id="input-clientu" class="form-control mb-3">
                                        @foreach($listClient as $client)
                                        <option value="{{ $client['email'] }}" >{{ $client['firstname'] }} {{ $client['lastname'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Obra de Teatro</label>
                                    <select name="theaterplayu" id="input-theaterplayu" class="form-control mb-3">
                                        @foreach($listTheaterplay as $theaterplay)
                                        <option value="{{ $theaterplay->id }}">{{ $theaterplay->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Butacas</label>
                                    <input type="text" name="dateu" id="input-dateu" class="form-control" readonly="true" value="" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fila</label>
                                    <select name="rowu[]" id="input-rowu" class="form-control mb-3">
                                        @for ($i = 0; $i < 5; $i++)
                                        <option value="{{ $i+1 }}">{{ $i+1 }}</option>
                                        @endfor    
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Columna</label>
                                    <select name="columnu[]" id="input-columnu" class="form-control mb-3">
                                        @for ($i = 0; $i < 10; $i++)
                                        <option value="{{ $i+1 }}" >{{ $i+1 }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-success" id="button-update-seat" >Agregar Butaca</button> -->
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                        <!-- <button type="button" class="btn btn-primary" id="button-update-reservation" data-object="" data-result="{{ $errorRegister }}">Guardar</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script src="{{ asset('js/complement-reservation.js') }}"></script>
@endsection