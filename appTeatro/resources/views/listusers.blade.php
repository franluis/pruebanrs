@extends('layouts.app')

@section('title', 'Panel | TEATRO')

@section('main-navbar')
<header class="header">
    <nav class="navbar">

        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
          
                <!-- Navbar Header-->
                <div class="navbar-header">
                
                    <!-- Navbar Brand -->
                    <a href="{{ route('index') }}" class="navbar-brand d-none d-sm-inline-block">
                        <div class="brand-text d-none d-lg-inline-block"><span>Panel </span><strong>Teatro</strong></div>
                        <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>P|Teatro</strong></div>
                    </a>
                
                    <!-- Toggle Button-->
                    <a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                </div>

            </div>
        </div>
    </nav>
</header>
@endsection

@section('side-navbar')
<nav class="side-navbar">

    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            <img src="{{ asset('img/avatar_user.jpg') }}" alt="..." class="img-fluid rounded-circle">
        </div>
        <div class="title">
            <h1 class="h4">Sonia Silva</h1>
            <p>Bienvenida</p>
        </div>
    </div>

    <!-- Sidebar Navidation Menus-->
    <ul class="list-unstyled">
        <li><a href="{{ route('index') }}"> <i class="icon-home"></i>Inicio </a></li>
        <li class="active"><a href="{{ route('users.index') }}"> <i class="fa fa-users" aria-hidden="true"></i>Clientes </a></li>
        <li><a href="{{ route('reservation.index') }}"> <i class="fa fa-ticket" aria-hidden="true"></i>Reservas </a></li>
    </ul>
</nav>
@endsection

@section('content-inner')
<div class="content-inner">
    
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Gestion de Clientes</h2>
        </div>
    </header>

    <!-- Breadcrumb-->
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Clientes</li>
        </ul>
    </div>
    
    <section class="tables">   
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-close">
                            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-success"><i class="fa fa-user-plus" aria-hidden="true"></i></button>
                        </div>
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Listado de Clientes</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Sexo</th>
                                            <th>Telefono</th>
                                            <th>Email</th>
                                            <th>Estatus</th>
                                            <th colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($listClient as $client)
                                        <tr>
                                            <th scope="row">{{ $client['id'] }}</th>
                                            <td>{{ $client['firstname'] }}</td>
                                            <td>{{ $client['lastname'] }}</td>
                                            @if($client['sex']=='F')
                                            <td><i class="fa fa-2x fa-female" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-2x fa-male" aria-hidden="true"></i></td>
                                            @endif
                                            <td>{{ $client['phone'] }}</td>
                                            <td>{{ $client['email'] }}</td>
                                            @if($client['status'])
                                            <td><div class="badge badge-rounded bg-green">Activo</div></td>
                                            @else
                                            <td><div class="badge badge-rounded bg-gray">Inactivo</div></td>
                                            @endif
                                            <td><button type="button" class="btn btn-xs btn-warning" id="button-update_{{ $client['id'] }}" data-object="{{ $client }}" data-toggle="modal" data-target="#myModalUpdate" ><i class="fa fa-pencil" aria-hidden="true"></i></button></td>
                                            <td><button type="button" class="btn btn-xs btn-danger" id="button-delete_{{ $client['id'] }}" data-object="{{ $client }}"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      
    <!-- Page Footer-->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>NRS GROUP &copy; 2018</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Desarrollado por <a href="https://www.linkedin.com/in/francisco-gerardino/" class="external">Francisco Gerardino</a></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modal-->
    <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('users.store') }}" >
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Nuevo Cliente</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="firstname" id="input-firstname" placeholder="Ingrese su nombre" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" name="lastname" id="input-lastname" placeholder="Ingrese su apellido" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <br/>
                                    <div class="row text-center">
                                        <div class="col-md-6">
                                            <label class="checkbox-inline">
                                                <input id="input-sexf" name="sex" type="radio" value="F"><i class="fa fa-3x fa-female" id="img-sexf" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="checkbox-inline">
                                                <input id="input-sexm" name="sex" type="radio" value="M"><i class="fa fa-3x fa-male" id="img-sexm" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <input type="text" name="phone" id="input-phone" placeholder="02931241212" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" id="input-email" placeholder="email@correo.com" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" id="input-password" placeholder="XXXXXXXX" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="button-register-client" data-result="{{ $errorRegister }}">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Mensaje Registro -->
    <div class="modal fade" id="modal-messaje-exito" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messaje-register"></h5>
                    <button type="button" class="btn btn-success" id="btn-messaje-register" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Mensaje Eliminar -->
    <div class="modal fade" id="modal-messaje-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messaje-delete"></h5>
                    <button type="button" class="btn btn-danger" id="button-delete-client" data-id="" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Modificar -->
    <div id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <form method="" >
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 id="exampleModalLabel" class="modal-title">Modificar Cliente</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="firstnameu" id="input-firstnameu" placeholder="Ingrese su nombre" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" name="lastnameu" id="input-lastnameu" placeholder="Ingrese su apellido" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <br/>
                                    <div class="row text-center">
                                        <div class="col-md-6">
                                            <label class="checkbox-inline">
                                                <input id="input-sexfu" name="sexu" type="radio" value="F"><i class="fa fa-3x fa-female" id="img-sexf" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="checkbox-inline">
                                                <input id="input-sexmu" name="sexu" type="radio" value="M"><i class="fa fa-3x fa-male" id="img-sexm" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <input type="text" name="phoneu" id="input-phoneu" placeholder="02931241212" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="emailu" id="input-emailu" placeholder="email@correo.com" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="passwordu" id="input-passwordu" placeholder="XXXXXXXX" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="button-update-client" data-object="" data-result="{{ $errorRegister }}">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection

@section('js')
<script src="{{ asset('js/complement-client.js') }}"></script>
@endsection