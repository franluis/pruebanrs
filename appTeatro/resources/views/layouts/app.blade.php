<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layouts.header')
    </head>
    <body>
        <div class="page">
            <!-- INICIO MAIN NAVBAR -->
            @yield('main-navbar')
            <!-- FIN MAIN NAVBAR -->

            <div class="page-content d-flex align-items-stretch"> 

                <!-- INICIO SIDE NAVBAR -->
                @yield('side-navbar')
                <!-- FIN SIDE NAVBAR -->
                
                <!-- INICIO CONTENT INNER -->
                @yield('content-inner')
                <!-- FIN CONTENT INNER -->

            </div>
        </div>

        @include('layouts.footer')

        <!-- INICIO JS -->
        @yield('js')
        <!-- FIN JS-->
    </body>
</html>