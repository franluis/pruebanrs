<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email_users', 100);
            $table->integer('id_theaterplay')->unsigned();
            $table->datetime('date');
            $table->enum('status', ['Registrada', 'Aprobada', 'Rechazada']);
            $table->timestamps();

            $table->foreign('email_users')->references('email')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_theaterplay')->references('id')->on('theaterplay')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation');
    }
}
