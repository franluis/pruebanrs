<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_reservation')->unsigned();
            $table->integer('id_theaterplay')->unsigned();
            $table->enum('row', [1, 2, 3, 4, 5]);
            $table->enum('column', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
            $table->timestamps();

            $table->foreign('id_reservation')->references('id')->on('reservation')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_theaterplay')->references('id')->on('theaterplay')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat');
    }
}
