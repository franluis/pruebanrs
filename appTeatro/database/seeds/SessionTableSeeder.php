<?php

use Illuminate\Database\Seeder;

class SessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('session')->insert(array(
        	array(
                'email_users' => 'franluisgb@gmail.com',
                'id_profile' => 1,
                'password' => bcrypt('123456'),
        		'created_at' => $date,
        		'updated_at' => $date
        	),
        	array(
        		'email_users' => 'jelianad@gmail.com',
                'id_profile' => 2,
                'password' => bcrypt('123456'),
        		'created_at' => $date,
        		'updated_at' => $date
        	),
        	array(
        		'email_users' => 'josetorrens@gmail.com',
                'id_profile' => 2,
                'password' => bcrypt('123456'),
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
