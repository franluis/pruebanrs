<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('profile')->insert(array(
        	array(
        		'name' => 'Administrador',
        		'created_at' => $date,
        		'updated_at' => $date
        	),
        	array(
        		'name' => 'Cliente',
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
