<?php

use Illuminate\Database\Seeder;

class SeatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('seat')->insert(array(
        	array(
                'id_reservation' => 1,
                'id_theaterplay' => 1,
                'row' => 1,
                'column' => 1,
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
                'id_reservation' => 1,
                'id_theaterplay' => 1,
                'row' => 1,
                'column' => 5,
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
                'id_reservation' => 1,
                'id_theaterplay' => 1,
                'row' => 1,
                'column' => 6,
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
