<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('users')->insert(array(
        	array(
                'email' => 'franluisgb@gmail.com',
                'firstname' => 'Francisco',
                'lastname' => 'Gerardino',
                'sex' => 'M',
                'phone' => '+584142921759',
                'status' => true,
        		'created_at' => $date,
        		'updated_at' => $date
        	),
        	array(
        		'email' => 'jelianad@gmail.com',
                'firstname' => 'Eliana',
                'lastname' => 'Diaz',
                'sex' => 'F',
                'phone' => '+584128408040',
                'status' => true,
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
        		'email' => 'josetorrens@gmail.com',
                'firstname' => 'Jose',
                'lastname' => 'Torrens',
                'sex' => 'M',
                'phone' => '+584121112233',
                'status' => true,
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
