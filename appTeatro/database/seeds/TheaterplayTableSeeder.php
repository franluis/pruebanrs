<?php

use Illuminate\Database\Seeder;

class TheaterplayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('theaterplay')->insert(array(
        	array(
                'name' => 'La casa de Bernarda Alba',
                'author' => 'Federico García Lorca',
                'date' => $date,
        		'created_at' => $date,
        		'updated_at' => $date
        	),
        	array(
                'name' => 'La vida es sueño',
                'author' => 'Pedro Calderón de la Barca',
                'date' => $date,
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
                'name' => 'Casa de muñecas',
                'author' => 'Henrik Ibsen',
                'date' => $date,
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
                'name' => 'El jardín de los cerezos',
                'author' => 'Antón Chéjov',
                'date' => $date,
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
