<?php

use Illuminate\Database\Seeder;

class ReservationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date('Y-m-d H:m:s');
        DB::table('reservation')->insert(array(
        	array(
                'email_users' => 'jelianad@gmail.com',
                'id_theaterplay' => 1,
                'date' => date('2018-09-17'),
                'status' => 'Registrada',
        		'created_at' => $date,
        		'updated_at' => $date
            ),
            array(
                'email_users' => 'josetorrens@gmail.com',
                'id_theaterplay' => 2,
                'date' => date('2018-09-17'),
                'status' => 'Registrada',
        		'created_at' => $date,
        		'updated_at' => $date
        	)
    	));
    }
}
