<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Theaterplay extends Model
{
    //
    protected $table = 'theaterplay';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'author', 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function theaterplayactive($date)
    {
        return DB::table('theaterplay')
                    ->where('date', '>', $date)
                    ->get();
    }
}
