<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User;
use App\Theaterplay;
use App\Seat;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $date = date('Y-m-d H:m:s');
        $reservation = new Reservation();
        $theaterplay = new Theaterplay();
        $listReservation = $reservation->reservationuserobra();
        $listTheaterplay = $theaterplay->theaterplayactive($date);
        $listClient = User::all();

        return view('listreservation')->with(['listReservation' => $listReservation, 'listClient' => $listClient, 'listTheaterplay' => $listTheaterplay, 'dateCurrent' => $date, 'errorRegister' => null, 'status' => 200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $reservation = new Reservation();

        $reservation->email_users = $request->client;
        $reservation->id_theaterplay = $request->theaterplay;
        $reservation->date = $request->date;
        $reservation->status = 'Registrada';

        $reservationSeat = 0;

        foreach ($request->row as $key => $value)
        {
            $seat = new Seat();

            $seat->id_reservation = Reservation::all()->count();
            $seat->id_theaterplay = $request->theaterplay;
            $seat->row = $request->row[$key];
            $seat->column = $request->column[$key];

            $reservationSeat = $seat->search($seat->row, $seat->column, $seat->id_theaterplay)->count() + $reservationSeat;
        }

        if($reservationSeat==0)
        {
            $reservation->save();

            foreach ($request->row as $key => $value)
            {
                $seat = new Seat();

                $seat->id_reservation = Reservation::all()->count();
                $seat->id_theaterplay = $request->theaterplay;
                $seat->row = $request->row[$key];
                $seat->column = $request->column[$key];

                $seat->save();
            }

            $date = date('Y-m-d H:m:s');
            $reservation = new Reservation();
            $theaterplay = new Theaterplay();
            $listReservation = $reservation->reservationuserobra();
            $listTheaterplay = $theaterplay->theaterplayactive($date);
            $listClient = User::all();

            return view('listreservation')->with(['listReservation' => $listReservation, 'listClient' => $listClient, 'listTheaterplay' => $listTheaterplay, 'dateCurrent' => $date, 'errorRegister' => 0, 'status' => 200]);
        }
        else
        {
            $date = date('Y-m-d H:m:s');
            $reservation = new Reservation();
            $theaterplay = new Theaterplay();
            $listReservation = $reservation->reservationuserobra();
            $listTheaterplay = $theaterplay->theaterplayactive($date);
            $listClient = User::all();

            return view('listreservation')->with(['listReservation' => $listReservation, 'listClient' => $listClient, 'listTheaterplay' => $listTheaterplay, 'dateCurrent' => $date, 'errorRegister' => 1, 'status' => 200]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $reservation = Reservation::find($id);

        if($reservation)
        {
            $reservation->delete();

            $data = $reservation;

            return response()->json(['data' => $data, 'errorDelete' => false, 'status' => 200]);
        }
        else
        {
            return response()->json(['data' => $reservation, 'errorDelete' => true, 'status' => 200]);
        }
    }
}
