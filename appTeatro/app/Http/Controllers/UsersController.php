<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $listClient = User::all();

        return view('listusers')->with(['listClient' => $listClient, 'errorRegister' => null, 'status' => 200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function create()
    {
        //
    } */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $userClient = User::where('email', $request->email)->first();

        if(!$userClient) {
            $user = new User;
            $session = new Session;

            $user->email = $request->email;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->sex = $request->sex;
            $user->phone = $request->phone;

            $session->email_users = $request->email;
            $session->id_profile = 2;
            $session->password = bcrypt($request->password);

            $user->save();
            $session->save();

            $listClient = User::all();

            return view('listusers')->with(['listClient' => $listClient, 'errorRegister' => 'false']);
        }
        else {

            $listClient = User::all();

            return view('listusers')->with(['listClient' => $listClient, 'errorRegister' => 'true']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $client = User::find($request->id);

        if($client)
        {
            $client->email = $request->email;
            $client->firstname = $request->firstname;
            $client->lastname = $request->lastname;
            $client->sex = $request->sex;
            $client->phone = $request->phone;

            $client->save();

            return response()->json(['data' => $client, 'errorUpdate' => false, 'status' => 200]);
        }
        else
        {
            return response()->json(['data' => null, 'errorUpdate' => true, 'status' => 200]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = User::find($id);

        if($client)
        {
            $client->delete();

            $data = $client;

            return response()->json(['data' => $data, 'errorDelete' => false, 'status' => 200]);
        }
        else
        {
            return response()->json(['data' => $client, 'errorDelete' => true, 'status' => 200]);
        }
    }
}
