<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Reservation;
use App\Theaterplay;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $countUsers = User::all()->count();
        $countReservation = Reservation::all()->count();
        $listTheaterplay = Theaterplay::all();

        return view('dashboard')->with(['countUsers' => $countUsers, 'countReservations' => $countReservation, 'listTheaterplay' => $listTheaterplay, 'status' => 200]);
    }
}
