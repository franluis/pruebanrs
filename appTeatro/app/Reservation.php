<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reservation extends Model
{
    //
    protected $table = 'reservation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_users', 'id_theaterplay', 'date', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function reservationuserobra()
    {
        return DB::table('reservation')
                    ->select('reservation.*', 'users.firstname', 'users.lastname', 'theaterplay.name')
                    ->leftJoin('users', 'reservation.email_users', '=', 'users.email')
                    ->leftJoin('theaterplay', 'reservation.id_theaterplay', '=', 'theaterplay.id')
                    ->orderBy('reservation.id', 'asc')
                    ->get();
    }

    public function getUserTheaterplayToIdReservation($id)
    {
        return DB::table('reservation')
                    ->select('users.firstname', 'users.lastname', 'users.email', 'theaterplay.name', 'reservation.id_theaterplay')
                    ->where('reservation.id', '=', $id)
                    ->leftJoin('users', 'reservation.email_users', '=', 'users.email')
                    ->leftJoin('theaterplay', 'reservation.id_theaterplay', '=', 'theaterplay.id')
                    ->get();
    }
}
