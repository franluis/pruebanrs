<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Seat extends Model
{
    //
    protected $table = 'seat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_reservation', 'id_theaterplay', 'row', 'column'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];

    public function search($row, $column, $theaterplay)
    {
        return DB::table('seat')
                    ->where('id_theaterplay', '=', $theaterplay)
                    ->where('row', '=', $row)
                    ->where('column', '=', $column)
                    ->get();
    }

    public function getToIdReservation($idReservation)
    {
        return DB::table('seat')
                    ->where('id_reservation', '=', $idReservation)
                    ->get();
    }
}
