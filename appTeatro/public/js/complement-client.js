/*
	Proyecto: appTeatro
	Fecha: 15/09/2018;
	Autor: Francisco Gerardino
-------------------------------------------------------------------------
-------------------------------------------------------------------------
*/

$(document).ready(function()
{	
	// Validation of the fields of the registration form
	$('#button-register-client').on('click', function()
	{
		let err = false;

		if(commonValidation.isValidInput(($('#input-firstname').val())))
		{
			$('#input-firstname').addClass('is-invalid');
			$('#input-firstname').focus();
			
			err = true;
		}
		else
		{
			$('#input-firstname').removeClass('is-invalid');
			$('#input-firstname').addClass('is-valid');
		}

		if(commonValidation.isValidInput(($('#input-lastname').val())))
		{
			$('#input-lastname').addClass('is-invalid');
			$('#input-lastname').focus();

			err = true;
		}
		else
		{
			$('#input-lastname').removeClass('is-invalid');
			$('#input-lastname').addClass('is-valid');
		}		
		
		if((!commonValidation.isValidRadio(($('#input-sexm'))))&&(!commonValidation.isValidRadio(($('#input-sexf')))))
		{
			$('#img-sexf').css('color', 'red');
			$('#img-sexm').css('color', 'red');

			err = true;
		}
		else
		{
			$('#img-sexf').css('color', 'black');
			$('#img-sexm').css('color', 'black');
		}

		if(commonValidation.isValidInput(($('#input-phone').val())))
		{
			$('#input-phone').addClass('is-invalid');
			$('#input-phone').focus();

			err = true;
		}		
		else
		{
			$('#input-phone').removeClass('is-invalid');
			$('#input-phone').addClass('is-valid');
		}

		if(!commonValidation.isValidEmail($('#input-email').val()))
		{
			$('#input-email').addClass('is-invalid');
			$('#input-email').focus();
			
			err = true;
		}
		else
		{
			$('#input-email').removeClass('is-invalid');
			$('#input-email').addClass('is-valid');
		}

		if(commonValidation.isValidInput(($('#input-password').val())))
		{
			$('#input-password').addClass('is-invalid');
			$('#input-password').focus();
			
			err = true;
		}
		else
		{
			$('#input-password').removeClass('is-invalid');
			$('#input-password').addClass('is-valid');
		}

		if(err)
		{
			return false;
		}
		else
		{
			return true;
		}
	});

	// Confirm Delete client
	$('[id^=button-delete_]').on('click', function()
	{
		$('#messaje-delete').text('Esta seguro de eliminar al usuario '+$(this).data('object').firstname+'?');
		$('#button-delete-client').attr('data-id', $(this).data('object').id);
		$('#modal-messaje-delete').modal('show');
	});

	// Delete client
	$('#button-delete-client').on('click', function()
	{
		let id = $('#button-delete-client').attr('data-id');

		$.ajax({
			type: 'DELETE',
			url: '/users/'+id,
			data: id,
			dataType: 'json',
			headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
			success: function(data)
			{
				console.log(data);
				if(!data.errorDelete)
				{
					$('#messaje-register').text('Registro Eliminado Correctamente');
					$('#btn-messaje-register').addClass('btn-success');
					$('#modal-messaje-exito').modal('show');

					window.location.href = "/users";
				}
				else
				{
					$('#messaje-register').text('Error en eliminacion de Registro');
					$('#btn-messaje-register').addClass('btn-warning');
					$('#modal-messaje-exito').modal('show');
				}
			},
			error: function(err)
			{
				$('#messaje-register').text('Error en eliminacion de Registro');
				$('#btn-messaje-register').addClass('btn-warning');
				$('#modal-messaje-exito').modal('show');
			}
		});
	});

	// Confirm Update client
	$('[id^=button-update_]').on('click', function()
	{
		$('#input-firstnameu').val($(this).data('object').firstname);
		$('#input-lastnameu').val($(this).data('object').lastname);
		$('#input-phoneu').val($(this).data('object').phone);
		$('#input-emailu').val($(this).data('object').email);
		$('#input-emailu').parent().parent().removeClass('col-md-6');
		$('#input-emailu').parent().parent().addClass('col-md-12');
		$('#input-passwordu').parent().remove();
		if($(this).data('object').sex==='F')
		{
			$('#input-sexfu').prop('checked', true);
		}
		else
		{
			$('#input-sexmu').prop('checked', true);
		}
		$('#button-update-client').attr('data-id', $(this).data('object').id);
	});

	// Update client
	$('#button-update-client').on('click', function()
	{
		let client = {
			"id": $('#button-update-client').attr('data-id'),
			"firstname": $('#input-firstnameu').val(),
			"lastname": $('#input-lastnameu').val(),
			'sex': $('#input-sexfu').is(':checked') ? 'F': 'M',
			"phone": $('#input-phoneu').val(),
			"email": $('#input-emailu').val()
		};

		$.ajax({
			type: 'PUT',
			url: '/users/'+client.id,
			data: client,
			dataType: 'json',
			headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
			success: function(data)
			{
				$('#myModalUpdate').modal('hide');

				if(!data.errorUpdate)
				{
					$('#messaje-register').text('Registro Actualizado Correctamente');
					$('#btn-messaje-register').addClass('btn-success');
					$('#modal-messaje-exito').modal('show');

					window.location.href = "/users";
				}
				else
				{
					$('#messaje-register').text('Error en actualizacion de Registro');
					$('#btn-messaje-register').addClass('btn-warning');
					$('#modal-messaje-exito').modal('show');
				}
			},
			error: function(err)
			{
				$('#myModalUpdate').modal('hide');
				
				$('#messaje-register').text('Error en actualizacion de Registro');
				$('#btn-messaje-register').addClass('btn-warning');
				$('#modal-messaje-exito').modal('show');
			}
		});
	});

	if($('#button-register-client').data('result')===true)
	{
		$('#messaje-register').text('Error en Registro. Usuario registrado anteriormente');
		$('#btn-messaje-register').addClass('btn-warning');
		$('#modal-messaje-exito').modal('show');
	}
	else if($('#button-register-client').data('result')===false)
	{
		$('#messaje-register').text('Registro Completado');
		$('#btn-messaje-register').addClass('btn-success');
		$('#modal-messaje-exito').modal('show');
	}
});