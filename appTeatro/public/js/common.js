/*
	Proyecto: appTeatro
	Fecha: 15/09/2018;
	Autor: Francisco Gerardino
-------------------------------------------------------------------------
-------------------------------------------------------------------------
*/

/* Definition of functions for validation */

commonValidation =
{
    /**
     * Function to validate email
     * @param {email} value 
     * @return true | false
     */
    isValidEmail: function(value)
    {
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value))
        {
            return true;
        }
        return false;
    },

    /**
     * Function to validate length of an input
     * @param {input} value 
     * @return true | false
     */
    isValidInput: function(value)
    {
        if (!value.length)
        {
            return true;
        }
        return false;
    },

    /**
     * Function to validate input type radio
     * @param {radio} value 
     * @return true | false
     */
    isValidRadio: function(value)
    {
        return value.is(':checked');
    },

    /**
     * Function to validate select
     * @param {select} value 
     * @return true | false
     */
    isValidSelect: function(value)
    {
        if (!value)
        {
            return true;
        }
        return false;
    }
}