/*
	Proyecto: appTeatro
	Fecha: 15/09/2018;
	Autor: Francisco Gerardino
-------------------------------------------------------------------------
-------------------------------------------------------------------------
*/

$(document).ready(function()
{
	//Validate theaterplay list
	$('#btn-register-reservation').on('click', function(){
		if($(this).data('length')===0){
			console.log("OCULTAR OBRAS DE ARTE");
			$('#messaje-validate').text('No hay Obras de Teatro disponibles');
			$('#modal-messaje-validate').modal('show');
			return false;
		}
		return true;
	})
	
	// Agregate seat
	$('#button-agregate-seat').on('click', function()
	{		
		let element = '<div class="row"><div class="col-md-3"><div class="form-group"><label>Fila</label><select name="row[]" id="input-row" class="form-control mb-3"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div></div><div class="col-md-3"><div class="form-group"><label>Columna</label><select name="column[]" id="input-column" class="form-control mb-3"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div></div></div>';
		
		$('#body-register-reservation').append(element);
	});

	// Agregate seat
	$('#button-update-seat').on('click', function()
	{		
		let element = '<div class="row"><div class="col-md-6"><div class="form-group"><label>Fila</label><select name="rowu[]" id="input-rowu" class="form-control mb-6"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div></div><div class="col-md-6"><div class="form-group"><label>Columna</label><select name="column[]" id="input-columnu" class="form-control mb-6"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div></div></div>';
		
		$('#body-update-reservation').append(element);
	});

	// Confirm Delete reservation
	$('[id^=button-delete_]').on('click', function()
	{
		$('#messaje-delete').text('Esta seguro de eliminar la reserva numero '+$(this).data('object')+'?');
		$('#button-delete-reservation').attr('data-id', $(this).data('object'));
		$('#modal-messaje-delete').modal('show');
	});

	// Delete reservation
	$('#button-delete-reservation').on('click', function()
	{
		let id = $('#button-delete-reservation').attr('data-id');
		
		$.ajax({
			type: 'DELETE',
			url: '/reservation/'+id,
			data: id,
			dataType: 'json',
			headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
			success: function(data)
			{
				if(!data.errorDelete)
				{
					$('#messaje-register').text('Registro Eliminado Correctamente');
					$('#btn-messaje-register').addClass('btn-success');
					$('#modal-messaje-exito').modal('show');

					window.location.href = "/reservation";
				}
				else
				{
					$('#messaje-register').text('Error en eliminacion de Registro');
					$('#btn-messaje-register').addClass('btn-warning');
					$('#modal-messaje-exito').modal('show');
				}
			},
			error: function(err)
			{
				$('#messaje-register').text('Error en eliminacion de Registro');
				$('#btn-messaje-register').addClass('btn-warning');
				$('#modal-messaje-exito').modal('show');
			}
		});
	});

	// Detail reservation
	$('[id^=button-detail_]').on('click', function()
	{
		let idReservation = $(this).data('object')
		
		$.ajax({
			type: 'GET',
			url: '/seat/'+idReservation,
			data: idReservation,
			dataType: 'json',
			headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()},
			success: function(data)
			{
				let value = "";

				data.data.listSeat.forEach(element => {
					value = value + element.row+'-'+element.column+" | ";
				});
				
				$('#input-clientu').val(data.data.reservation[0].email);
				$('#input-theaterplayu').val(data.data.reservation[0].id_theaterplay);
				$('#input-dateu').val(value);

			},
			error: function(err)
			{
				cosole.log("Error en lista de butacas");
			}
		});
	});

	if($('#button-register-reservation').data('result')===1)
	{
		$('#messaje-register').text('Error en Registro. Butaca ocupada');
		$('#btn-messaje-register').addClass('btn-warning');
		$('#modal-messaje-exito').modal('show');
	}
	else if($('#button-register-reservation').data('result')===0)
	{
		$('#messaje-register').text('Reserva Completada Exitosamente');
		$('#btn-messaje-register').addClass('btn-success');
		$('#modal-messaje-exito').modal('show');
	}
});